﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EncrypterLib
{
    public class Encrypter
    {
        private static Random random = new Random();
        private string characters = "";
        private int complexity = 5;
        private int keySize = 3;

        public Encrypter(int complexity)
        {
            characters = "";

            for(int i = 0; i < 256; i++)
            {
                characters += (char)i;
            }

            if (complexity > 3000000)
                complexity = 3000000;

            if (complexity < 1)
                complexity = 1;

            keySize = (characters.Length * complexity).ToString().Length;
        }

        public string Decrypt(string original)
        {
            string newStr = "";

            try
            {
                if (original.Length < complexity * keySize)
                    return "";

                string[][] keyStrings = GetKeyStrings();
                string key = original.Substring(0, complexity * keySize);
                string encoded = original.Substring(complexity * keySize);

                //offsets determine the "rotation"  of the inner wheels.
                int[] offsets = new int[complexity];

                for (int i = 0; i < offsets.Length; i++)
                {
                    offsets[i] = int.Parse(key.Substring(0, keySize));
                    key = key.Substring(keySize);
                }

                while (encoded.Length > 0)
                {
                    if (encoded.Length < keySize)
                        break;

                    string currChar = encoded.Substring(0, keySize);
                    encoded = encoded.Substring(keySize);

                    for (int i = 0; i < keyStrings.Length; i++)
                    {
                        int rowOffset = offsets[i];

                        for (int j = 0; j < keyStrings[i].Length; j++)
                        {
                            int index = (j + rowOffset) % keyStrings[i].Length;

                            while (index < 0)
                                index = keyStrings[i].Length + index;

                            if (keyStrings[i][index].Equals(currChar))
                            {
                                newStr += characters[j];
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.Error.Write(e.Message);
            }

            return newStr;
        }

        //encrypt a message. Automagically prepends (randomly picked) message cipher key to the encrypted message.
        public string Encrypt(string original)
        {
            string[][] keyStrings = GetKeyStrings();
            string newStr = "";

            string key = "";

            for (int i = 0; i < complexity; i++)
            {
                for (int o = 0; o < keySize; o++)
                {
                    key += Math.Floor((random.NextDouble() * 10)).ToString();
                }
            }

            //offsets determine the "rotation"  of the inner wheels.
            int[] offsets = new int[complexity];

            newStr += key;

            for (int i = 0; i < offsets.Length; i++)
            {
                offsets[i] = int.Parse(key.Substring(0, keySize));
                key = key.Substring(keySize);
            }

            for (int i = 0; i < original.Length; i++)
            {
                int row = random.Next() % complexity;
                int index = characters.IndexOf(original[i]) + offsets[row];

                newStr += keyStrings[row][index % characters.Length];
            }

            return newStr;
        }

        //Populates a multi-dimentional array with the characters in the character string, and numbers in the "inner wheels". Not rotated.
        private string[][] GetKeyStrings()
        {
            string[][] strings = new string[complexity][];

            for (int i = 0; i < strings.Length; i++)
            {
                strings[i] = new string[characters.Length];

                for (int s = 0; s < characters.Length; s++)
                {
                    int num = (characters.Length * i) + s;

                    for (int o = 0; o < keySize; o++)
                        strings[i][s] += "0";

                    strings[i][s] += num.ToString();

                    strings[i][s] = strings[i][s].Substring(strings[i][s].Length - (keySize + 0), keySize);
                }
            }

            return strings;
        }
    }
}

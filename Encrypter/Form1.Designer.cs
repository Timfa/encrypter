﻿namespace Encrypter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ClearTextbox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.DecryptState = new System.Windows.Forms.RadioButton();
            this.EncryptState = new System.Windows.Forms.RadioButton();
            this.EncryptedTextbox = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ClearTextbox
            // 
            this.ClearTextbox.AcceptsReturn = true;
            this.ClearTextbox.Dock = System.Windows.Forms.DockStyle.Top;
            this.ClearTextbox.Location = new System.Drawing.Point(0, 0);
            this.ClearTextbox.Multiline = true;
            this.ClearTextbox.Name = "ClearTextbox";
            this.ClearTextbox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ClearTextbox.Size = new System.Drawing.Size(909, 190);
            this.ClearTextbox.TabIndex = 0;
            this.ClearTextbox.TextChanged += new System.EventHandler(this.ClearTextbox_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBox1.Controls.Add(this.DecryptState);
            this.groupBox1.Controls.Add(this.EncryptState);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 190);
            this.groupBox1.MaximumSize = new System.Drawing.Size(0, 60);
            this.groupBox1.MinimumSize = new System.Drawing.Size(0, 60);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(909, 60);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // DecryptState
            // 
            this.DecryptState.AutoSize = true;
            this.DecryptState.Location = new System.Drawing.Point(7, 42);
            this.DecryptState.Name = "DecryptState";
            this.DecryptState.Size = new System.Drawing.Size(62, 17);
            this.DecryptState.TabIndex = 1;
            this.DecryptState.Text = "Decrypt";
            this.DecryptState.UseVisualStyleBackColor = true;
            this.DecryptState.CheckedChanged += new System.EventHandler(this.DecryptState_CheckedChanged);
            // 
            // EncryptState
            // 
            this.EncryptState.AutoSize = true;
            this.EncryptState.Checked = true;
            this.EncryptState.Location = new System.Drawing.Point(7, 7);
            this.EncryptState.Name = "EncryptState";
            this.EncryptState.Size = new System.Drawing.Size(61, 17);
            this.EncryptState.TabIndex = 0;
            this.EncryptState.TabStop = true;
            this.EncryptState.Text = "Encrypt";
            this.EncryptState.UseVisualStyleBackColor = true;
            this.EncryptState.CheckedChanged += new System.EventHandler(this.EncryptState_CheckedChanged);
            // 
            // EncryptedTextbox
            // 
            this.EncryptedTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EncryptedTextbox.BackColor = System.Drawing.SystemColors.HighlightText;
            this.EncryptedTextbox.Location = new System.Drawing.Point(0, 251);
            this.EncryptedTextbox.Name = "EncryptedTextbox";
            this.EncryptedTextbox.Size = new System.Drawing.Size(909, 247);
            this.EncryptedTextbox.TabIndex = 3;
            this.EncryptedTextbox.Text = "";
            this.EncryptedTextbox.TextChanged += new System.EventHandler(this.EncryptedTextbox_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 495);
            this.Controls.Add(this.EncryptedTextbox);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ClearTextbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Encrypter";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox ClearTextbox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton DecryptState;
        private System.Windows.Forms.RadioButton EncryptState;
        private System.Windows.Forms.RichTextBox EncryptedTextbox;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Encrypter
{
    public partial class Form1 : Form
    {
        private EncrypterLib.Encrypter encrypter = new EncrypterLib.Encrypter(5);

        public Form1()
        {
            InitializeComponent();

            //Rich text boxes have weird backgrounds, so I recolor it to look normal.
            EncryptedTextbox.ReadOnly = true;

            EncryptedTextbox.BackColor = Form.DefaultBackColor;
        }

        //Form controls start here

        private void EncryptState_CheckedChanged(object sender, EventArgs e)
        {
            if (EncryptState.Checked)
            {
                DecryptState.Checked = false;

                EncryptedTextbox.ReadOnly = true;
                ClearTextbox.ReadOnly = false;
                EncryptedTextbox.BackColor = Form.DefaultBackColor;
            }
        }

        private void DecryptState_CheckedChanged(object sender, EventArgs e)
        {
            if (DecryptState.Checked)
            {
                EncryptState.Checked = false;

                EncryptedTextbox.ReadOnly = false;
                ClearTextbox.ReadOnly = true;
                EncryptedTextbox.BackColor = Color.White;
            }
        }

        private void ClearTextbox_TextChanged(object sender, EventArgs e)
        {
            if (EncryptState.Checked)
            {
                EncryptedTextbox.Text = encrypter.Encrypt(ClearTextbox.Text);
            }
        }

        private void EncryptedTextbox_TextChanged(object sender, EventArgs e)
        {
            if (DecryptState.Checked)
            {
                ClearTextbox.Text = encrypter.Decrypt(EncryptedTextbox.Text);
            }
        }
    }
}
